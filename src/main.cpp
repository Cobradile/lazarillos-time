#include "iostream"
#include "vector"

#include "bn_memory.h"
#include "bn_log.h"
#include "bn_core.h"
#include "bn_keypad.h"
#include "bn_string_view.h"
#include "bn_vector.h"
#include "bn_sprite_text_generator.h"

// Core
#include "scene.h"
#include "seeker.h"
#include "interface.h"

// Entities
#include "npc.h"
#include "foe.h"
#include "templatform.h"
#include "warp.h"

int main()
{
    // Initialization
    bn::core::init();
    
    enum State {
        TITLE, START, SCENE, PAUSE, TRANS
    };

    // First thing when opening the game: TITLE SCREEN (demo: SCENE)
    State state = START;
    Scene* scene = new Scene();
    Seeker* seeker = new Seeker(scene);
    std::vector<Entity*> trash;

    while(true)
    {
        // Decides controls based on the current status (type of screen)
        switch(state) {

            case START:

                // Initialization of the Player according to the Map
                delete seeker;
                seeker = NULL;
                seeker = new Seeker(scene);
                seeker->LoadScene();
                state = SCENE;

                break;

            case SCENE:

                // PLAYER & SCENE MANAGEMENT
                // 1. Update the player
                seeker->Update();
                // 2. Constantly set the camera to the player's position
                scene->SetCameraPos(seeker->GetPos(true), seeker->GetPos(false)-16);
                // 3. Update the scene's backgrounds
                scene->Update();

                // NPC MANAGEMENT
                // 1. Update every foe in the active list; every departed NPC will go to the trash for imminent deletion
                for(NPC* npc : scene->npcs) {
                    npc->Update();
                    if (npc->departed) trash.push_back(npc);
                }
                // 2. Remove all departed NPCs from active list
                scene->npcs.erase(
                    std::remove_if(
                        scene->npcs.begin(), scene->npcs.end(), [](NPC* npc) {
                            return npc->departed;
                        }
                    ), scene->npcs.end()
                );

                // BLOCKED DURING DIALOGUE
                if (!seeker->interface->dialoguing) {

                    // TEMPLATFORM MANAGEMENT
                    // 1. Update every templatform in the active list; every departed templatform will go to the trash for imminent deletion
                    for(Templatform* temp : scene->temps) {
                        temp->Update();
                    }

                    // FOE MANAGEMENT
                    // 1. Update every foe in the active list
                    for(Foe* foe : scene->foes) {
                        foe->Update();
                        if (foe->GetHP() <= 0) trash.push_back(foe);
                    }
                    // 2. Remove all foes with HP = 0 from active list
                    scene->foes.erase(
                        std::remove_if(
                            scene->foes.begin(), scene->foes.end(), [](Foe* foe) {
                                return foe->GetHP() <= 0;
                            }
                        ), scene->foes.end()
                    );

                    // WARP MANAGEMENT
                    // 1. Update every warp in the active list
                    for(Warp* warp : scene->warps) warp->Update();
                    // 2. If collision is found, warp
                    if (seeker->warped) {
                        seeker->LoadScene();
                    }

                    // PAUSE
                    if (bn::keypad::start_pressed()) {
                        state = PAUSE;
                        break;
                    }
                }

                // TRASH MANAGEMENT
                // 1. Delete all trashed entities
                for(Entity* e : trash) {
                    delete e;
                    e = NULL;
                }
                trash.clear();
                
                break;

            case PAUSE:

                // Open menu
                if (bn::keypad::start_pressed()) {
                    state = SCENE;
                    break;
                }

                break;
        }

        // Constant update at the end of the gameplay loop
        bn::core::update();
    }
}