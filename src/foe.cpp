#pragma once

#include "foe.h"
#include "seeker.h"

        Foe::Foe(bn::sprite_item p, int x, int y, Seeker *s) : Entity(p, x, y, s->GetScene()), seeker(s) {}

        void Foe::ChangeHP(int value) {
            if (state != HIT && state != KO) {
                if (value < 0) {
                    // Set flip according to position of Seeker
                    if (seeker->GetPos(true) < GetPos(true)) projection.set_horizontal_flip(true);
                    else projection.set_horizontal_flip(false);
                    // Set animation according to damage received
                    if (state == BLOCK) return;
                    else if (hp + value <= 0) Flinch(true);
                    else Flinch(false);
                }

                if (hp + value > 0) {
                    if (hp + value > hpMax) hp = hpMax;
                    else hp += value;
                }
            }
        }

        int Foe::GetHP() {
            return hp;
        }

        void Foe::SetHP(int h) {
            hpMax = h;
            hp = hpMax;
        }

        void Foe::Flinch(bool d) {
            if (!d) SetStateHit();
            else SetStateKO();

            if (overgrounded) overgrounded = false;
            if (grounded) velY = jumpSpeed/2;

            if (projection.horizontal_flip()) velX = speed;
            else velX = -speed;
        }

        void Foe::CommonUpdate() {
            if (seeker->EntityCollides(posX, posY, limitN, limitW, limitS, limitE) && state != HIT && state != KO) {
                if (seeker->GetPos(true) < posX) right = false;
                else right = true;

                seeker->Flip(right);
                seeker->ChangeHP(-1);
            }
            
            // Mid-air specifications
            if (!grounded) {
                // Apply gravity
                if (velY < maxVelY) velY += gravity;
            }

            // Land when needed
            if (landing) {
                focused = false;
                landing = false;
            }

            // End
            UpdatePos();
            UpdateState();
            CheckColTile();
        }

        void Foe::SetStateHit() {
            focused = false;
            ResetActions();

            state = HIT;
            focused = true;
            action2 = bn::create_sprite_animate_action_once(projection, 30, sprite.tiles_item(), hitIdx, hitIdx);
        }

        void Foe::SetStateKO() {
            focused = false;
            ResetActions();

            state = KO;
            focused = true;
            action5 = bn::create_sprite_animate_action_once(projection, 20, sprite.tiles_item(), deadIdx, deadIdx+1, deadIdx+2, deadIdx+3, deadIdx+3);
        }