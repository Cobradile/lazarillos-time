#pragma once

#include "interface.h"
#include "seeker.h"
        
        Interface::Interface() {
            coinText = new TextGenerator();
            msgText1 = new TextGenerator();
            msgText2 = new TextGenerator();
            msgText3 = new TextGenerator();
        }

        void Interface::Load(Seeker *s) {

            Clear();

            // Branch definition
            branch_l = lBranch.create_sprite(branchPosX,branchPosY);
            branch_r = rBranch.create_sprite(branchPosX+64,branchPosY);

            // HP definition
            if (s->hpMax >= 1) {
                if (s->hp >= 1) grape_0 = grapeOn.create_sprite(grapePosX[0],grapePosY[0]);
                else grape_0 = grapeOff.create_sprite(grapePosX[0],grapePosY[0]);
            }
            if (s->hpMax >= 2) {
                if (s->hp >= 2) grape_1 = grapeOn.create_sprite(grapePosX[1],grapePosY[1]);
                else grape_1 = grapeOff.create_sprite(grapePosX[1],grapePosY[1]);
            }
            if (s->hpMax >= 3) {
                if (s->hp >= 3) grape_2 = grapeOn.create_sprite(grapePosX[2],grapePosY[2]);
                else grape_2 = grapeOff.create_sprite(grapePosX[2],grapePosY[2]);
            }
            if (s->hpMax >= 4) {
                if (s->hp >= 4) grape_3 = grapeOn.create_sprite(grapePosX[3],grapePosY[3]);
                else grape_3 = grapeOff.create_sprite(grapePosX[3],grapePosY[3]);
            }
            if (s->hpMax >= 5) {
                if (s->hp >= 5) grape_4 = grapeOn.create_sprite(grapePosX[4],grapePosY[4]);
                else grape_4 = grapeOff.create_sprite(grapePosX[4],grapePosY[4]);
            }
            if (s->hpMax >= 6) {
                if (s->hp >= 6) grape_5 = grapeOn.create_sprite(grapePosX[5],grapePosY[5]);
                else grape_5 = grapeOff.create_sprite(grapePosX[5],grapePosY[5]);
            }
            if (s->hpMax >= 7) {
                if (s->hp >= 7) grape_6 = grapeOn.create_sprite(grapePosX[6],grapePosY[6]);
                else grape_6 = grapeOff.create_sprite(grapePosX[6],grapePosY[6]);
            }
            if (s->hpMax >= 8) {
                if (s->hp >= 8) grape_7 = grapeOn.create_sprite(grapePosX[7],grapePosY[7]);
                else grape_7 = grapeOff.create_sprite(grapePosX[7],grapePosY[7]);
            }
            if (s->hpMax >= 9) {
                if (s->hp >= 9) grape_8 = grapeOn.create_sprite(grapePosX[8],grapePosY[8]);
                else grape_8 = grapeOff.create_sprite(grapePosX[8],grapePosY[8]);
            }
            if (s->hpMax >= 10) {
                if (s->hp >= 10) grape_9 = grapeOn.create_sprite(grapePosX[9],grapePosY[9]);
                else grape_9 = grapeOff.create_sprite(grapePosX[9],grapePosY[9]);
            }

            // MP definition
            switch (s->mp) {
                case 8: jar = jar_8.create_sprite(jarPosX,jarPosY); break;
                case 7: jar = jar_7.create_sprite(jarPosX,jarPosY); break;
                case 6: jar = jar_6.create_sprite(jarPosX,jarPosY); break;
                case 5: jar = jar_5.create_sprite(jarPosX,jarPosY); break;
                case 4: jar = jar_4.create_sprite(jarPosX,jarPosY); break;
                case 3: jar = jar_3.create_sprite(jarPosX,jarPosY); break;
                case 2: jar = jar_2.create_sprite(jarPosX,jarPosY); break;
                case 1: jar = jar_1.create_sprite(jarPosX,jarPosY); break;
                case 0: jar = jar_0.create_sprite(jarPosX,jarPosY); break;
            }

            // Coin definition
            bag = coinBag.create_sprite(bagPosX,bagPosY);
            int zeros = 0;
            int tens = 10;
            while (s->coin >= tens) {
                zeros++;
                tens = tens * 10;
            }
            coinText->GenerateText(bagPosX-15-7*zeros, bagPosY+1, s->coin);

            // L&R item slots
            slot_l = slot.create_sprite(slotX, slotY);
            slot_r = slot.create_sprite(-slotX, slotY);
            button_l = lButton.create_sprite(slotX-8, slotY-8);
            button_r = rButton.create_sprite(-slotX+8, slotY-8);

            Order();
            
        }

        void Interface::Clear() {
            branch_l.reset(); branch_r.reset();
            grape_0.reset(); grape_1.reset(); grape_2.reset(); grape_3.reset(); grape_4.reset(); grape_5.reset(); grape_6.reset(); grape_7.reset(); grape_8.reset(); grape_9.reset();
            jar.reset();
            bag.reset();
            slot_l.reset(); slot_r.reset(); button_l.reset(); button_r.reset();
        }

        void Interface::Order() {
            if (branch_l.has_value()) {
                branch_l.get()->set_bg_priority(0); branch_l.get()->set_z_order(-2);
                branch_r.get()->set_bg_priority(0); branch_r.get()->set_z_order(-2);
            }
            if (grape_0.has_value()) {
                grape_0.get()->set_bg_priority(0); grape_0.get()->set_z_order(-3);
            }
            if (grape_1.has_value()) {
                grape_1.get()->set_bg_priority(0); grape_1.get()->set_z_order(-3);
            }
            if (grape_2.has_value()) {
                grape_2.get()->set_bg_priority(0); grape_2.get()->set_z_order(-3);
            }
            if (grape_3.has_value()) {
                grape_3.get()->set_bg_priority(0); grape_3.get()->set_z_order(-3);
            }
            if (grape_4.has_value()) {
                grape_4.get()->set_bg_priority(0); grape_4.get()->set_z_order(-3);
            }
            if (grape_5.has_value()) {
                grape_5.get()->set_bg_priority(0); grape_5.get()->set_z_order(-3);
            }
            if (grape_6.has_value()) {
                grape_6.get()->set_bg_priority(0); grape_6.get()->set_z_order(-3);
            }
            if (grape_7.has_value()) {
                grape_7.get()->set_bg_priority(0); grape_7.get()->set_z_order(-3);
            }
            if (grape_8.has_value()) {
                grape_8.get()->set_bg_priority(0); grape_8.get()->set_z_order(-3);
            }
            if (grape_9.has_value()) {
                grape_9.get()->set_bg_priority(0); grape_9.get()->set_z_order(-3);
            }
            if (jar.has_value()) {
                jar.get()->set_bg_priority(0); jar.get()->set_z_order(-3);
            }
            if (bag.has_value()) {
                bag.get()->set_bg_priority(0); bag.get()->set_z_order(-2);
            }
            if (slot_l.has_value()) {
                slot_l.get()->set_bg_priority(0); slot_l.get()->set_z_order(-2);
                button_l.get()->set_bg_priority(0); button_l.get()->set_z_order(-3);
            }
            if (slot_r.has_value()) {
                slot_r.get()->set_bg_priority(0); slot_r.get()->set_z_order(-2);
                button_r.get()->set_bg_priority(0); button_r.get()->set_z_order(-3);
            }
            if (box_left.has_value()) {
                box_left.get()->set_bg_priority(0); box_left.get()->set_z_order(-4);
                box_centerLeft.get()->set_bg_priority(0); box_centerLeft.get()->set_z_order(-4);
                box_centerRight.get()->set_bg_priority(0); box_centerRight.get()->set_z_order(-4);
                box_right.get()->set_bg_priority(0); box_right.get()->set_z_order(-4);
            }
        }

        void Interface::ReceiveMessage(bn::string_view msg, int lines) {
            dialoguing = true;

            // Create a textbox if it's the first line
            if (lines == 0) {
                box_left = lBox.create_sprite(boxX, boxY);
                box_centerLeft = cBox.create_sprite(boxX/3, boxY);
                box_centerRight = cBox.create_sprite(-boxX/3, boxY);
                box_right = rBox.create_sprite(-boxX, boxY);
                Order();
            }

            // Depending on the quantity of the currently shown lines, choose a line to write to
            switch(lines) {
                case 0: msgText1->GenerateText(boxX-24, boxY-20, msg); break;
                case 1: msgText2->GenerateText(boxX-24, boxY-8, msg); break;
                case 2: msgText3->GenerateText(boxX-24, boxY+4, msg); break;
            }
        }

        void Interface::EndTransmission() {
            dialoguing = false;
            
            // Delete textbox and messages
            box_left.reset(); box_centerLeft.reset(); box_centerRight.reset(); box_right.reset();
            msgText1->Clear(); msgText2->Clear(); msgText3->Clear();
        }