#pragma once

#include "foe_piedra.h"
#include "seeker.h"

        Piedra::Piedra(int x, int y, Seeker *s, bool r) : Foe(bn::sprite_items::s_piedra, x, y, s) {
            
            SetHP(1);

            velX = 1;
            velY = 0;
            speed = 2;

            right = r;

            limitN = 6;
            limitW = 6;
            limitS = 6;
            limitE = 6;

            projection.set_z_order(-1);
        }

        void Piedra::Update() {
            if (seeker->EntityCollides(posX, posY, limitN, limitW, limitS, limitE)) {
                if (velX > 0) seeker->Flip(true);
                else seeker->Flip(false);
                seeker->ChangeHP(-1);
                ChangeHP(-1);
            } else Move();
            
            // Apply gravity after the straight counter ends
            straightCounter--;
            if (straightCounter <= 0) {
                if (velY < maxVelY) velY += gravity;
            }

            // Destroy upon landing or hitting a wall
            if (landing || wBlocked || eBlocked) ChangeHP(-1);

            // End
            UpdatePos();
            UpdateState();
            CheckColTile();
        }

        void Piedra::Move() {
            projection.set_horizontal_flip(!right);
            if (right) velX = speed;
            else velX = -speed;
        }
        
        void Piedra::ChangeHP(int value) {
            if (hp + value >= hpMax) hp = 0;
            else if (hp + value >= hpMax) hp = hpMax;
            else hp += value;
        }