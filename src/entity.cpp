#pragma once

#include "entity.h"
#include "scene.h"

        Entity::Entity(bn::sprite_item p, int x, int y, Scene *sc) : sprite(p), projection(p.create_sprite(x,y)) {
            posX = projection.x();
            posY = projection.y();
            scene = sc;
            projection.set_bg_priority(0);
            projection.set_z_order(1);
            projection.set_camera(sc->GetCamera());
        }

        void Entity::CheckColTile() {

            // Type N (ceiling)
            if (velY < 0) {
                for (int lectorX = -limitW+1; lectorX < limitE-1; lectorX++) {
                    if (lectorX < 0) {
                        if (scene->GetOffsetTile(this, limitN, -lectorX, 0, 0) >= scene->bTiles[0] && scene->GetOffsetTile(this, limitN, -lectorX, 0, 0) <= scene->bTiles[1]) {
                            velY = 0;
                            break;
                        }
                    } else {
                        if (scene->GetOffsetTile(this, limitN, 0, 0, lectorX) >= scene->bTiles[0] && scene->GetOffsetTile(this, limitN, 0, 0, lectorX) <= scene->bTiles[1]) {
                            velY = 0;
                            break;
                        }
                    }
                }
            }

            // Type W (wall to the left)
            if (velX < 0 || wBlocked) {
                for (int lectorY = -limitN+1; lectorY < limitS-1; lectorY++) {
                    if (lectorY < 0) {
                        if (scene->GetOffsetTile(this, -lectorY, limitW, 0, 0) >= scene->bTiles[0] && scene->GetOffsetTile(this, -lectorY, limitW, 0, 0) <= scene->bTiles[1]) {
                            wBlocked = true;
                            velX = 0;
                            break;
                        }
                    } else {
                        if (scene->GetOffsetTile(this, 0, limitW, lectorY, 0) >= scene->bTiles[0] && scene->GetOffsetTile(this, 0, limitW, lectorY, 0) <= scene->bTiles[1]) {
                            wBlocked = true;
                            velX = 0;
                            break;
                        }
                    }
                    wBlocked = false;
                }
            }

            // Type S (floor)
            if (!overgrounded) {
                bool gTemp = false;
                if (velY >= 0) {
                    for (int lectorX = -limitW+1; lectorX < limitE-1; lectorX++) {
                        if (lectorX < 0) {
                            if (scene->GetOffsetTile(this, 0, -lectorX, limitS, 0) >= scene->bTiles[0] && scene->GetOffsetTile(this, 0, -lectorX, limitS, 0) <= scene->bTiles[1]) {
                                if (!grounded) landing = true;
                                gTemp = true;
                                velY = 0;
                                onPlatform = false;
                                break;
                            } else if (scene->pTiles[0] != 0 && scene->GetOffsetTile(this, 0, -lectorX, limitS, 0) >= scene->pTiles[0] && scene->GetOffsetTile(this, 0, -lectorX, limitS, 0) <= scene->pTiles[1]) {
                                if (!grounded) landing = true;
                                gTemp = true;
                                velY = 0;
                                onPlatform = true;
                                break;
                            }
                        } else {
                            if (scene->GetOffsetTile(this, 0, 0, limitS, lectorX) >= scene->bTiles[0] && scene->GetOffsetTile(this, 0, 0, limitS, lectorX) <= scene->bTiles[1]) {
                                if (!grounded) landing = true;
                                gTemp = true;
                                velY = 0;
                                onPlatform = false;
                                break;
                            } else if (scene->pTiles[0] != 0 && scene->GetOffsetTile(this, 0, 0, limitS, lectorX) >= scene->pTiles[0] && scene->GetOffsetTile(this, 0, 0, limitS, lectorX) <= scene->pTiles[1]) {
                                if (!grounded) landing = true;
                                gTemp = true;
                                velY = 0;
                                onPlatform = true;
                                break;
                            }
                        }
                    }
                }
                grounded = gTemp;
            } else if (velY >= 0) {
                if (!grounded) landing = true;
                grounded = true;
                velY = 0;
                onPlatform = true;
            }

            // Type E (wall to the right)
            if (velX > 0 || eBlocked) {
                for (int lectorY = -limitN+1; lectorY < limitS-1; lectorY++) {
                    if (lectorY < 0) {
                        if (scene->GetOffsetTile(this, -lectorY, 0, 0, limitE) >= scene->bTiles[0] && scene->GetOffsetTile(this, -lectorY, 0, 0, limitE) <= scene->bTiles[1]) {
                            eBlocked = true;
                            velX = 0;
                            break;
                        }
                    } else {
                        if (scene->GetOffsetTile(this, 0, 0, lectorY, limitE) >= scene->bTiles[0] && scene->GetOffsetTile(this, 0, 0, lectorY, limitE) <= scene->bTiles[1]) {
                            eBlocked = true;
                            velX = 0;
                            break;
                        }
                    }
                    eBlocked = false;
                }
            }
        }

        ActionState Entity::GetState() {
            return state;
        }

        bool Entity::EntityCollides(bn::fixed entPosX, bn::fixed entPosY, int entLimitN, int entLimitW, int entLimitS, int entLimitE) {
            bool trueX = false;
            bool trueY = false;

            if (entPosX-entLimitW <= posX+limitE && entPosX+entLimitE >= posX-limitW) trueX = true;
            if (entPosY-entLimitN <= posY+limitS && entPosY+entLimitS >= posY-limitN) trueY = true;

            if (trueX && trueY) return true;
            else return false;
        }