#pragma once

#include "scene.h"
#include "seeker.h"

// Foes
#include "foe_zafio.h"
#include "foe_zafiolanzador.h"
#include "foe_perro.h"

// NPCs
#include "npc_madre.h"

// Other entities
#include "templatform.h"
#include "warp.h"

        Scene::Scene() {}

        void Scene::Load(int i, int x, int y, Seeker *s) {

            index = i;
            startX = x;
            startY = y;

            tilemap.reset();
            foreground.reset();
            background.reset();

            switch(index) {
                // STAGE 1 -------------------------------------------------
                // Home
                case 1: SetScene(bn::music_items::stage1,
                    bn::affine_bg_items::tm_stage1_a1, 8, 0, 0, 7,
                    bn::regular_bg_items::fg_river, 128, 0, 0.04f,
                    bn::regular_bg_items::bg_sky, 128, 32, 0.02f); break;
                // Tutorial A
                case 2: SetScene(bn::music_items::stage1,
                    bn::affine_bg_items::tm_stage1_a2, 8, 0, 0, 8,
                    bn::regular_bg_items::fg_river, 128, 0, 0.04f,
                    bn::regular_bg_items::bg_sky, 128, 32, 0.02f); break;
                // Tutorial B
                case 3: SetScene(bn::music_items::stage1,
                    bn::affine_bg_items::tm_stage1_a5, 8, 0, 0, 8,
                    bn::regular_bg_items::fg_river, 128, 0, 0.04f,
                    bn::regular_bg_items::bg_sky, 128, 32, 0.02f); break;
                // ---------------------------------------------------------
            }

            RegenerateEntities(s);

            if (melody.has_value()) {
                if (newMelody) melody.get()->play();
            } else bn::music::pause();
            
            if (tilemap.has_value()) {
                tilemap.get()->set_position(tilemap.get()->dimensions().width()/2, tilemap.get()->dimensions().height()/2);
                tilemap.get()->set_priority(1);
                tilemap.get()->set_camera(camera);
            }

            if (foreground.has_value()) foreground.get()->set_priority(2);

            if (background.has_value()) background.get()->set_priority(3);
        }

        void Scene::SetScene(bn::music_item m, bn::affine_bg_item t, int tB, int tS, int tL, int tP, bn::regular_bg_item f, int fX, int fY, float fP, bn::regular_bg_item b, int bX, int bY, float bP) {
            SetBGM(m);

            tilemap = t.create_bg(0,0);
            SetTiles(tB, tS, tL, tP);

            foreground = f.create_bg(fX,fY);
            fgFactor = fP;

            background = b.create_bg(bX,bY);
            bgFactor = bP;
        }

        void Scene::SetSceneT(bn::music_item m, bn::affine_bg_item t, int tB, int tS, int tL, int tP) {
            SetBGM(m);

            tilemap = t.create_bg(0,0);
            SetTiles(tB, tS, tL, tP);
        }

        void Scene::SetSceneF(bn::music_item m, bn::affine_bg_item t, int tB, int tS, int tL, int tP, bn::regular_bg_item f, int fX, int fY, float fP) {
            SetBGM(m);

            tilemap = t.create_bg(0,0);
            SetTiles(tB, tS, tL, tP);

            foreground = f.create_bg(fX,fY);
            fgFactor = fP;
        }

        void Scene::SetSceneB(bn::music_item m, bn::affine_bg_item t, int tB, int tS, int tL, int tP, bn::regular_bg_item b, int bX, int bY, float bP) {
            SetBGM(m);

            tilemap = t.create_bg(0,0);
            SetTiles(tB, tS, tL, tP);

            background = b.create_bg(bX,bY);
            bgFactor = bP;
        }

        void Scene::SetTiles(int bQty, int sQty, int lQty, int pQty) {
            int acc = 0;

            // Block tiles
            bTiles.clear();
            if (bQty != 0) {
                bTiles.push_back(1 + acc);
                bTiles.push_back(bQty + acc);
            } else bTiles.push_back(0);
            acc += bQty;

            // Spike tiles
            sTiles.clear();
            if (sQty != 0) {
                sTiles.push_back(1 + acc);
                sTiles.push_back(sQty + acc);
            } else sTiles.push_back(0);
            acc += sQty;
             
            // Ladder tiles
            lTiles.clear();
            if (lQty != 0) {
                lTiles.push_back(1 + acc);
                lTiles.push_back(lQty + acc);
            } else lTiles.push_back(0);
            acc += lQty;

            // Platform tiles
            pTiles.clear();
            if (pQty != 0) {
                pTiles.push_back(1 + acc);
                pTiles.push_back(pQty + acc);
            } else pTiles.push_back(0);
            acc += pQty;
        }

        void Scene::Update() {
            // Stop camera on tilemap border
            if      (camera.x() <= 120)                                     camera.set_x(120);
            else if (camera.x() >= tilemap.get()->dimensions().width()-120) camera.set_x(tilemap.get()->dimensions().width()-120);
            if      (camera.y() <= 80)                                      camera.set_y(80);
            else if (camera.y() >= tilemap.get()->dimensions().height()-80) camera.set_y(tilemap.get()->dimensions().height()-80);

            // Calc distance between last frame's camera and this frame's camera
            float disX = camera.x().to_float() - camX.to_float();
            float disY = camera.y().to_float() - camY.to_float();

            // Update background position according to parallax factor
            if (foreground.has_value()) foreground.get()->set_position(foreground.get()->x() - disX * fgFactor, foreground.get()->y() - disY * fgFactor);
            if (background.has_value()) background.get()->set_position(background.get()->x() - disX * bgFactor, background.get()->y() - disY * bgFactor);

            // Store this frame's camera
            camX = camera.x();
            camY = camera.y();
        }

        int Scene::GetIndex() {
            return index;
        }

        bn::fixed Scene::GetStartPos(bool x) {
            if (x) return startX;
            else return startY;
        }

        void Scene::RegenerateEntities(Seeker *s) {
            ClearEntities();
            switch(index) {
                case 1:
                    // Warps
                    AddWarp(new Warp(248,448,s,2,64,430));
                    // NPCs
                    AddNPC(new Madre(104,416,s));
                    break;
                case 2:
                    // Warps
                    AddWarp(new Warp(8,448,s,1,192,430));
                    // Foes
                    AddFoe(new Zafio(130,316,s));
                    AddFoe(new Perro(480,416,s));
                    break;
                case 3:
                    break;
            }
        }

        void Scene::ClearEntities() {
            for(Foe* foe : foes) {
                delete foe;
                foe = NULL;
            }
            foes.clear();

            for(NPC* npc : npcs) {
                delete npc;
                npc = NULL;
            }
            npcs.clear();

            for(Warp* warp : warps) {
                delete warp;
                warp = NULL;
            }
            warps.clear();

            for(Templatform* temp : temps) {
                delete temp;
                temp = NULL;
            }
            temps.clear();
        }

        void Scene::AddFoe(Foe* f) {
            foes.push_back(f);
        }

        void Scene::AddNPC(NPC* n) {
            npcs.push_back(n);
        }

        void Scene::AddWarp(Warp* w) {
            warps.push_back(w);
        }

        void Scene::AddTemp(Templatform* t) {
            temps.push_back(t);
        }

        bn::fixed Scene::GetIndex(Entity *entity) {
            return (entity->GetPos(true)/8).unsigned_integer() + (entity->GetPos(false)/8).unsigned_integer() * tilemap.get()->dimensions().width()/8;
        }

        int Scene::GetTile(Entity *entity) {
            return tilemap.get()->map().cells_ref().value().at(GetIndex(entity).round_integer());
        }

        bn::fixed Scene::GetOffsetIndex(Entity *entity, int n, int w, int s, int e) {
            return ((entity->GetPos(true)-w+e)/8).unsigned_integer() + ((entity->GetPos(false)-n+s)/8).unsigned_integer() * tilemap.get()->dimensions().width()/8;
        }

        int Scene::GetOffsetTile(Entity *entity, int n, int w, int s, int e) {
            return tilemap.get()->map().cells_ref().value().at(GetOffsetIndex(entity, n, w, s, e).round_integer());
        }

        bn::camera_ptr Scene::GetCamera() {
            return camera;
        }

        void Scene::SetCameraPos(bn::fixed x, bn::fixed y) {
            camera.set_x(x);
            camera.set_y(y);
        }

        void Scene::SetBGM(bn::music_item newBGM) {
            if (melody == newBGM) newMelody = false;
            else {
                newMelody = true;
                melody = newBGM;
            }
        }

        namespace std {
            void __throw_length_error(char const*) {}
        }