#pragma once

#include "npc.h"
#include "seeker.h"
#include "interface.h"

        NPC::NPC(bn::sprite_item p, int x, int y, Seeker *s) : Entity(p, x, y, s->GetScene()), seeker(s) {}

        void NPC::Update() {
            // NPC specifics
            if (bn::keypad::up_pressed() && seeker->EntityCollides(posX, posY, limitN, limitW, limitS, limitE)) {
                if (posX > seeker->GetPos(true)) {
                    seeker->Flip(false);
                    projection.set_horizontal_flip(true);
                } else {
                    seeker->Flip(true);
                    projection.set_horizontal_flip(false);
                }
                Interact();
            }

            if (!seeker->interface->dialoguing) {
                // Entity commons
                if (!grounded) {
                    // Apply gravity
                    if (velY < maxVelY) velY += gravity;
                }
                UpdatePos();
                CheckColTile();
            }
            UpdateState();
        }

        void NPC::Interact() {

            // If there are max lines showing, clear textbox
            if (shownLines >= maxLines) {
                seeker->interface->EndTransmission();
                shownLines = 0;
            }

            // If the current index is LOWER than the size of the dialogues, start sending messages 1 by 1
            if (index < dialogues1.size()) {
                while (shownLines < maxLines && index < dialogues1.size()) {
                    seeker->interface->ReceiveMessage(dialogues1[index], shownLines);
                    // Increase the number of lines showing
                    shownLines++;
                    // Increase the reading index
                    index++;
                }
            // If the current index is NOT LOWER to the dialogues size: end interaction, reset counters and change the dialogues
            } else {
                seeker->interface->EndTransmission();
                shownLines = 0;
                index = 0;
                dialogues1 = dialogues2;
            }

            // Event check for special NPCs
            EventCheck();
        }

        void NPC::EventCheck() {}

        void NPC::SetAnim(int idx) {}