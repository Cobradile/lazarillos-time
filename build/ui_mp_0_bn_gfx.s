
@{{BLOCK(ui_mp_0_bn_gfx)

@=======================================================================
@
@	ui_mp_0_bn_gfx, 16x16@4, 
@	+ palette 16 entries, not compressed
@	+ 4 tiles not compressed
@	Total size: 32 + 128 = 160
@
@	Time-stamp: 2024-08-10, 14:04:12
@	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global ui_mp_0_bn_gfxTiles		@ 128 unsigned chars
	.hidden ui_mp_0_bn_gfxTiles
ui_mp_0_bn_gfxTiles:
	.word 0x03333000,0x3BAAA300,0x97BBA300,0x99773000,0x89955300,0x21111300,0x02213000,0x20001300
	.word 0x00000000,0x00000003,0x00000039,0x00000038,0x00003394,0x00039000,0x00396600,0x03906000
	.word 0x40001300,0x00008300,0x00068300,0x00068300,0x00683000,0x66830000,0x18300000,0x33000000
	.word 0x03500002,0x03500000,0x03800000,0x03130000,0x00313000,0x00031336,0x00003111,0x00000333

	.section .rodata
	.align	2
	.global ui_mp_0_bn_gfxPal		@ 32 unsigned chars
	.hidden ui_mp_0_bn_gfxPal
ui_mp_0_bn_gfxPal:
	.hword 0x0000,0x3DE4,0x2EDF,0x0067,0x15F6,0x6F6F,0x7FFF,0x052F
	.hword 0x5E68,0x7FF3,0x0DFA,0x0170,0x0000,0x0000,0x0000,0x0000

@}}BLOCK(ui_mp_0_bn_gfx)
