#ifndef BN_SPRITE_ITEMS_WARP_H
#define BN_SPRITE_ITEMS_WARP_H

#include "bn_sprite_item.h"

//{{BLOCK(warp_bn_gfx)

//======================================================================
//
//	warp_bn_gfx, 16x16@8, 
//	+ palette 48 entries, not compressed
//	+ 4 tiles not compressed
//	Total size: 96 + 256 = 352
//
//	Time-stamp: 2024-08-10, 14:04:12
//	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_WARP_BN_GFX_H
#define GRIT_WARP_BN_GFX_H

#define warp_bn_gfxTilesLen 256
extern const bn::tile warp_bn_gfxTiles[8];

#define warp_bn_gfxPalLen 96
extern const bn::color warp_bn_gfxPal[48];

#endif // GRIT_WARP_BN_GFX_H

//}}BLOCK(warp_bn_gfx)

namespace bn::sprite_items
{
    constexpr inline sprite_item warp(sprite_shape_size(sprite_shape::SQUARE, sprite_size::NORMAL), 
            sprite_tiles_item(span<const tile>(warp_bn_gfxTiles, 8), bpp_mode::BPP_8, compression_type::NONE, 1), 
            sprite_palette_item(span<const color>(warp_bn_gfxPal, 48), bpp_mode::BPP_8, compression_type::NONE));
}

#endif

