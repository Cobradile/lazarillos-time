
@{{BLOCK(ui_mp_2_bn_gfx)

@=======================================================================
@
@	ui_mp_2_bn_gfx, 16x16@4, 
@	+ palette 16 entries, not compressed
@	+ 4 tiles not compressed
@	Total size: 32 + 128 = 160
@
@	Time-stamp: 2024-08-10, 14:04:12
@	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global ui_mp_2_bn_gfxTiles		@ 128 unsigned chars
	.hidden ui_mp_2_bn_gfxTiles
ui_mp_2_bn_gfxTiles:
	.word 0x04444000,0x4EDDD400,0xC8EED400,0xCC884000,0x9CC33400,0x52222400,0x05524000,0x50002400
	.word 0x00000000,0x00000004,0x0000004C,0x00000049,0x000044C6,0x0004C000,0x004C7700,0x04C07000
	.word 0x60002400,0x00009400,0x00079400,0xB0079400,0xBB794000,0x77940000,0x29400000,0x44000000
	.word 0x04300005,0x04300000,0x049ABB00,0x042ABBBB,0x0042AAAB,0x00042A11,0x00004222,0x00000444

	.section .rodata
	.align	2
	.global ui_mp_2_bn_gfxPal		@ 32 unsigned chars
	.hidden ui_mp_2_bn_gfxPal
ui_mp_2_bn_gfxPal:
	.hword 0x0000,0x02AA,0x3DE4,0x6F6F,0x0067,0x2EDF,0x15F6,0x7FFF
	.hword 0x052F,0x5E68,0x03F5,0x2BFF,0x7FF3,0x0DFA,0x0170,0x0000

@}}BLOCK(ui_mp_2_bn_gfx)
