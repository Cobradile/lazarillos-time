
@{{BLOCK(p_gone_2_bn_gfx)

@=======================================================================
@
@	p_gone_2_bn_gfx, 32x16@4, 
@	+ palette 16 entries, not compressed
@	+ 8 tiles not compressed
@	Total size: 32 + 256 = 288
@
@	Time-stamp: 2024-08-10, 14:04:12
@	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global p_gone_2_bn_gfxTiles		@ 256 unsigned chars
	.hidden p_gone_2_bn_gfxTiles
p_gone_2_bn_gfxTiles:
	.word 0x00000000,0x00000000,0x00000000,0x10000000,0x10000000,0x00000000,0x00000000,0x00000000
	.word 0x00000000,0x00000000,0x00005555,0x25052444,0x33305233,0x33100111,0x11000000,0x00000000
	.word 0x00000000,0x00000000,0x55000000,0x44550000,0x24441000,0x32231000,0x11110055,0x00000014
	.word 0x00000000,0x00000000,0x00000000,0x00000001,0x00000001,0x00000001,0x00000000,0x00000000
	.word 0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x00000013,0x00000001,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000

	.section .rodata
	.align	2
	.global p_gone_2_bn_gfxPal		@ 32 unsigned chars
	.hidden p_gone_2_bn_gfxPal
p_gone_2_bn_gfxPal:
	.hword 0x0000,0x00EB,0x01D4,0x0151,0x0E97,0x00CD,0x0000,0x0000
	.hword 0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000

@}}BLOCK(p_gone_2_bn_gfx)
