
@{{BLOCK(ui_mp_7_bn_gfx)

@=======================================================================
@
@	ui_mp_7_bn_gfx, 16x16@4, 
@	+ palette 16 entries, not compressed
@	+ 4 tiles not compressed
@	Total size: 32 + 128 = 160
@
@	Time-stamp: 2024-08-10, 14:04:12
@	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global ui_mp_7_bn_gfxTiles		@ 128 unsigned chars
	.hidden ui_mp_7_bn_gfxTiles
ui_mp_7_bn_gfxTiles:
	.word 0x05555000,0x5EDDD500,0xC9EED500,0xCC995000,0xACC33500,0x62222500,0x06625000,0x60BB2500
	.word 0x00000000,0x00000005,0x0000005C,0x0000005A,0x000055C4,0x0005C000,0x005C7700,0x05CB7BB0
	.word 0x4BB82500,0xBB88A500,0x8887A500,0x8887A500,0x887A5000,0x77A50000,0x2A500000,0x55000000
	.word 0x05388BB6,0x0538888B,0x05A88888,0x05218888,0x00521888,0x00052111,0x00005222,0x00000555

	.section .rodata
	.align	2
	.global ui_mp_7_bn_gfxPal		@ 32 unsigned chars
	.hidden ui_mp_7_bn_gfxPal
ui_mp_7_bn_gfxPal:
	.hword 0x0000,0x02AA,0x3DE4,0x6F6F,0x15F6,0x0067,0x2EDF,0x7FFF
	.hword 0x03F5,0x052F,0x5E68,0x2BFF,0x7FF3,0x0DFA,0x0170,0x0000

@}}BLOCK(ui_mp_7_bn_gfx)
