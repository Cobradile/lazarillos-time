#ifndef BN_MUSIC_ITEMS_H
#define BN_MUSIC_ITEMS_H

#include "bn_music_item.h"

namespace bn::music_items
{
    constexpr inline music_item stage1(0);
    constexpr inline music_item stage2(1);
    constexpr inline music_item stage3(2);
}

#endif

