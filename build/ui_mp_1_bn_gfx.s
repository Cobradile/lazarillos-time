
@{{BLOCK(ui_mp_1_bn_gfx)

@=======================================================================
@
@	ui_mp_1_bn_gfx, 16x16@4, 
@	+ palette 16 entries, not compressed
@	+ 4 tiles not compressed
@	Total size: 32 + 128 = 160
@
@	Time-stamp: 2024-08-10, 14:04:12
@	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global ui_mp_1_bn_gfxTiles		@ 128 unsigned chars
	.hidden ui_mp_1_bn_gfxTiles
ui_mp_1_bn_gfxTiles:
	.word 0x04444000,0x4DCCC400,0xB7DDC400,0xBB774000,0x8BB33400,0x21111400,0x02214000,0x20001400
	.word 0x00000000,0x00000004,0x0000004B,0x00000048,0x000044B5,0x0004B000,0x004B6600,0x04B06000
	.word 0x50001400,0x00008400,0x00068400,0x00068400,0x00684000,0x66840000,0x18400000,0x44000000
	.word 0x04300002,0x04300000,0x04800000,0x041A0000,0x0041A000,0x000419AA,0x00004111,0x00000444

	.section .rodata
	.align	2
	.global ui_mp_1_bn_gfxPal		@ 32 unsigned chars
	.hidden ui_mp_1_bn_gfxPal
ui_mp_1_bn_gfxPal:
	.hword 0x0000,0x3DE4,0x2EDF,0x6F6F,0x0067,0x15F6,0x7FFF,0x052F
	.hword 0x5E68,0x03F5,0x2BFF,0x7FF3,0x0DFA,0x0170,0x0000,0x0000

@}}BLOCK(ui_mp_1_bn_gfx)
