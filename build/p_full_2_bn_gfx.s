
@{{BLOCK(p_full_2_bn_gfx)

@=======================================================================
@
@	p_full_2_bn_gfx, 32x16@4, 
@	+ palette 16 entries, not compressed
@	+ 8 tiles not compressed
@	Total size: 32 + 256 = 288
@
@	Time-stamp: 2024-08-10, 14:04:12
@	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global p_full_2_bn_gfxTiles		@ 256 unsigned chars
	.hidden p_full_2_bn_gfxTiles
p_full_2_bn_gfxTiles:
	.word 0x35000000,0x25000000,0x40000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x33333333,0x33333222,0x22225222,0x22225125,0x11544150,0x24111100,0x51441000,0x11100000
	.word 0x33333333,0x22253333,0x22252222,0x52155222,0x05144411,0x00111144,0x00014415,0x00000111
	.word 0x00000053,0x00000052,0x00000004,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000

	.section .rodata
	.align	2
	.global p_full_2_bn_gfxPal		@ 32 unsigned chars
	.hidden p_full_2_bn_gfxPal
p_full_2_bn_gfxPal:
	.hword 0x0000,0x00EB,0x01D4,0x0E97,0x0151,0x00CD,0x0000,0x0000
	.hword 0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000

@}}BLOCK(p_full_2_bn_gfx)
