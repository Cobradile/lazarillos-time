
@{{BLOCK(p_hurt_2_bn_gfx)

@=======================================================================
@
@	p_hurt_2_bn_gfx, 32x16@4, 
@	+ palette 16 entries, not compressed
@	+ 8 tiles not compressed
@	Total size: 32 + 256 = 288
@
@	Time-stamp: 2024-08-10, 14:04:12
@	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global p_hurt_2_bn_gfxTiles		@ 256 unsigned chars
	.hidden p_hurt_2_bn_gfxTiles
p_hurt_2_bn_gfxTiles:
	.word 0x35000000,0x25000000,0x40000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x55333333,0x33333222,0x12555222,0x55335125,0x54441150,0x11100100,0x10000000,0x00000000
	.word 0x33315000,0x22251505,0x22252551,0x52155111,0x05141111,0x00100001,0x00000001,0x00000001
	.word 0x00000053,0x00000052,0x00000004,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000

	.section .rodata
	.align	2
	.global p_hurt_2_bn_gfxPal		@ 32 unsigned chars
	.hidden p_hurt_2_bn_gfxPal
p_hurt_2_bn_gfxPal:
	.hword 0x0000,0x00EB,0x01D4,0x0E97,0x0151,0x00CD,0x0000,0x0000
	.hword 0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000

@}}BLOCK(p_hurt_2_bn_gfx)
