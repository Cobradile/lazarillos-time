
@{{BLOCK(ui_bgm_on_bn_gfx)

@=======================================================================
@
@	ui_bgm_on_bn_gfx, 16x16@4, 
@	+ palette 16 entries, not compressed
@	+ 4 tiles not compressed
@	Total size: 32 + 128 = 160
@
@	Time-stamp: 2024-08-10, 14:04:12
@	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global ui_bgm_on_bn_gfxTiles		@ 128 unsigned chars
	.hidden ui_bgm_on_bn_gfxTiles
ui_bgm_on_bn_gfxTiles:
	.word 0x55555550,0x33333335,0x11111165,0x11111165,0x44411165,0x11411165,0x44411165,0x11411165
	.word 0x05555555,0x53333333,0x56111111,0x56114444,0x56114111,0x56114441,0x56114114,0x56114111
	.word 0x11411165,0x11441165,0x11444165,0x11444165,0x11111165,0x22222265,0x22222265,0x55555550
	.word 0x56114411,0x56114441,0x56114441,0x56111111,0x56111111,0x56222222,0x56222222,0x05555555

	.section .rodata
	.align	2
	.global ui_bgm_on_bn_gfxPal		@ 32 unsigned chars
	.hidden ui_bgm_on_bn_gfxPal
ui_bgm_on_bn_gfxPal:
	.hword 0x0000,0x0DFA,0x010F,0x26DF,0x00AB,0x0067,0x0170,0x0000
	.hword 0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000

@}}BLOCK(ui_bgm_on_bn_gfx)
