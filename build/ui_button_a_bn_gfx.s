
@{{BLOCK(ui_button_a_bn_gfx)

@=======================================================================
@
@	ui_button_a_bn_gfx, 16x16@4, 
@	+ palette 16 entries, not compressed
@	+ 4 tiles not compressed
@	Total size: 32 + 128 = 160
@
@	Time-stamp: 2024-08-10, 14:04:12
@	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global ui_button_a_bn_gfxTiles		@ 128 unsigned chars
	.hidden ui_button_a_bn_gfxTiles
ui_button_a_bn_gfxTiles:
	.word 0x33300000,0x44433000,0x22244300,0x22222430,0x55222430,0x25522213,0x22522213,0x55522213
	.word 0x00000333,0x00033444,0x00344222,0x03422222,0x03422255,0x31222552,0x31222522,0x31222555
	.word 0x22522213,0x22522213,0x22522213,0x22222130,0x22222130,0x22211300,0x66633000,0x33300000
	.word 0x31222522,0x31222522,0x31222522,0x03122222,0x03122222,0x00311222,0x00033666,0x00000333

	.section .rodata
	.align	2
	.global ui_button_a_bn_gfxPal		@ 32 unsigned chars
	.hidden ui_button_a_bn_gfxPal
ui_button_a_bn_gfxPal:
	.hword 0x0000,0x0170,0x0DFA,0x0067,0x26DF,0x00AB,0x010F,0x0000
	.hword 0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000

@}}BLOCK(ui_button_a_bn_gfx)
