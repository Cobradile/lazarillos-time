
@{{BLOCK(ui_button_r_bn_gfx)

@=======================================================================
@
@	ui_button_r_bn_gfx, 16x16@4, 
@	+ palette 16 entries, not compressed
@	+ 4 tiles not compressed
@	Total size: 32 + 128 = 160
@
@	Time-stamp: 2024-08-10, 14:04:12
@	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global ui_button_r_bn_gfxTiles		@ 128 unsigned chars
	.hidden ui_button_r_bn_gfxTiles
ui_button_r_bn_gfxTiles:
	.word 0x22222220,0x44444442,0x33333312,0x53333312,0x53333312,0x53333312,0x53333312,0x53333312
	.word 0x02222222,0x24444444,0x21333333,0x21333555,0x21333533,0x21333355,0x21333533,0x21333533
	.word 0x66666620,0x22222200,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000,0x00000000
	.word 0x21333333,0x21333336,0x21333362,0x21333362,0x21666620,0x02222200,0x00000000,0x00000000

	.section .rodata
	.align	2
	.global ui_button_r_bn_gfxPal		@ 32 unsigned chars
	.hidden ui_button_r_bn_gfxPal
ui_button_r_bn_gfxPal:
	.hword 0x0000,0x0170,0x0067,0x0DFA,0x26DF,0x00AB,0x010F,0x0000
	.hword 0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000

@}}BLOCK(ui_button_r_bn_gfx)
