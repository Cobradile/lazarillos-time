
@{{BLOCK(ui_mp_5_bn_gfx)

@=======================================================================
@
@	ui_mp_5_bn_gfx, 16x16@4, 
@	+ palette 16 entries, not compressed
@	+ 4 tiles not compressed
@	Total size: 32 + 128 = 160
@
@	Time-stamp: 2024-08-10, 14:04:12
@	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global ui_mp_5_bn_gfxTiles		@ 128 unsigned chars
	.hidden ui_mp_5_bn_gfxTiles
ui_mp_5_bn_gfxTiles:
	.word 0x05555000,0x5EDDD500,0xC8EED500,0xCC885000,0xACC33500,0x42222500,0x04425000,0x40002500
	.word 0x00000000,0x00000005,0x0000005C,0x0000005A,0x000055C9,0x0005C000,0x005C7700,0x05C07000
	.word 0x90002500,0xBBBBA500,0xBB67A500,0x6617A500,0x617A5000,0x77A50000,0x2A500000,0x55000000
	.word 0x05300004,0x053000BB,0x05A6BBBB,0x05266666,0x00526666,0x00052661,0x00005222,0x00000555

	.section .rodata
	.align	2
	.global ui_mp_5_bn_gfxPal		@ 32 unsigned chars
	.hidden ui_mp_5_bn_gfxPal
ui_mp_5_bn_gfxPal:
	.hword 0x0000,0x02AA,0x3DE4,0x6F6F,0x2EDF,0x0067,0x03F5,0x7FFF
	.hword 0x052F,0x15F6,0x5E68,0x2BFF,0x7FF3,0x0DFA,0x0170,0x0000

@}}BLOCK(ui_mp_5_bn_gfx)
