
@{{BLOCK(ui_coinbag_bn_gfx)

@=======================================================================
@
@	ui_coinbag_bn_gfx, 16x16@4, 
@	+ palette 16 entries, not compressed
@	+ 4 tiles not compressed
@	Total size: 32 + 128 = 160
@
@	Time-stamp: 2024-08-10, 14:04:12
@	Exported by Cearn's GBA Image Transmogrifier, v0.9.2
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global ui_coinbag_bn_gfxTiles		@ 128 unsigned chars
	.hidden ui_coinbag_bn_gfxTiles
ui_coinbag_bn_gfxTiles:
	.word 0x00000000,0x00000000,0x00000000,0x33000000,0x76300000,0x65300000,0x13000000,0x42300000
	.word 0x00000000,0x00000000,0x00000333,0x00003677,0x00003667,0x00003566,0x00000316,0x00003244
	.word 0x21300000,0x15530000,0x56553000,0x45553000,0x25513000,0x11130000,0x33300000,0x00000000
	.word 0x00003124,0x00036614,0x00356664,0x00365652,0x00316555,0x00031111,0x00003333,0x00000000

	.section .rodata
	.align	2
	.global ui_coinbag_bn_gfxPal		@ 32 unsigned chars
	.hidden ui_coinbag_bn_gfxPal
ui_coinbag_bn_gfxPal:
	.hword 0x0000,0x010F,0x15F6,0x0067,0x2EDF,0x05B4,0x0DFA,0x00AB
	.hword 0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000

@}}BLOCK(ui_coinbag_bn_gfx)
