#pragma once

#include "bn_sprite_items_s_calderero.h"

#include "npc.h"

class Seeker;

class Calderero : public NPC {

    public:

        Calderero(int x, int y, Seeker *s);

        void SetAnim(int idx) override;

        void UpdateState() override;
};