#pragma once

#include "entity.h"

class Seeker;

class Foe : public Entity {

    protected:

        // Foe data
        Seeker* seeker;
        int hpMax;
        int hp;

        // Action variables
        bool seekerDetected = false;
        bool limitFound = false;
        bool right = true;

        // Sprite
        int hitIdx = 0;
        int deadIdx = 0;


    public:

        Foe(bn::sprite_item p, int x, int y, Seeker *s);

        void ChangeHP(int value);

        int GetHP();

        void SetHP(int h);

        void Flinch(bool d);

        void CommonUpdate();

        void SetStateHit();

        void SetStateKO();
};