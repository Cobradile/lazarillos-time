#pragma once

#include "iostream"
#include "vector"

#include "bn_camera_actions.h"
#include "bn_regular_bg_ptr.h"
#include "bn_affine_bg_ptr.h"
#include "bn_affine_bg_map_ptr.h"
#include "bn_memory.h"

// Tilemaps
#include "bn_affine_bg_items_tm_stage1_a1.h"
#include "bn_affine_bg_items_tm_stage1_a2.h"
#include "bn_affine_bg_items_tm_stage1_a5.h"

// Backgrounds
#include "bn_regular_bg_items_bg_sky.h"
#include "bn_regular_bg_items_bg_forest.h"
#include "bn_regular_bg_items_bg_church.h"

// Foregrounds
#include "bn_regular_bg_items_fg_river.h"
#include "bn_regular_bg_items_fg_grass.h"

#include "bn_music.h"
#include "bn_music_items.h"

class Entity;
class Seeker;
class Foe;
class NPC;
class Warp;
class Templatform;

class Scene {

    protected:

        // Scene index
        int index;

        // Starting coordinates
        bn::fixed startX;
        bn::fixed startY;

        // Camera
        bn::camera_ptr camera = bn::camera_ptr::create(0, 0);
        bn::fixed camX = bn::fixed::from_data(0);
        bn::fixed camY = bn::fixed::from_data(0);
        float fgFactor = 1;
        float bgFactor = 1;

        // Graphic resources
        bn::optional<bn::affine_bg_ptr> tilemap;
        bn::optional<bn::regular_bg_ptr> foreground;
        bn::optional<bn::regular_bg_ptr> background;

        // Audio resources
        bn::optional<bn::music_item> melody;
        bool newMelody = true;

    public:

        // Tile types (by index)
        std::vector<int> bTiles {1, 8};
        std::vector<int> sTiles {0, 0};
        std::vector<int> lTiles {0, 0};
        std::vector<int> pTiles {9, 12};
        void SetTiles(int bQty, int sQty, int lQty, int pQty);

        // Entity lists
        std::vector<Foe*> foes;
        std::vector<NPC*> npcs;
        std::vector<Warp*> warps;
        std::vector<Templatform*> temps;

        Scene();

        void Load(int i, int x, int y, Seeker *s);

        void SetScene(bn::music_item m,
            bn::affine_bg_item t, int tB, int tS, int tL, int tP,
            bn::regular_bg_item f, int fX, int fY, float fP,
            bn::regular_bg_item b, int bX, int bY, float bP);

        void SetSceneT(bn::music_item m,
            bn::affine_bg_item t, int tB, int tS, int tL, int tP);

        void SetSceneF(bn::music_item m,
            bn::affine_bg_item t, int tB, int tS, int tL, int tP,
            bn::regular_bg_item b, int bX, int bY, float bP);

        void SetSceneB(bn::music_item m,
            bn::affine_bg_item t, int tB, int tS, int tL, int tP,
            bn::regular_bg_item f, int fX, int fY, float fP);

        int GetIndex();

        void Update();

        bn::fixed GetStartPos(bool x);

        void RegenerateEntities(Seeker *s);

        void ClearEntities();

        void AddFoe(Foe* f);

        void AddNPC(NPC* n);

        void AddWarp(Warp* w);

        void AddTemp(Templatform* t);

        bn::fixed GetIndex(Entity *entity);

        bn::fixed GetOffsetIndex(Entity *entity, int n, int w, int s, int e);

        int GetTile(Entity *entity);

        int GetOffsetTile(Entity *entity, int n, int w, int s, int e);

        bn::camera_ptr GetCamera();

        void SetCameraPos(bn::fixed x, bn::fixed y);

        void SetBGM(bn::music_item newBGM);
};