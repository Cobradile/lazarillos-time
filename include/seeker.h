#pragma once

#include "bn_camera_actions.h"
#include "bn_affine_bg_ptr.h"
#include "bn_sprite_items_s_seeker_bronze.h"
#include "bn_sprite_items_s_seeker_silver.h"
#include "bn_sprite_items_s_seeker_golden.h"

enum ItemSlot {
    NONE, SHIELD, BOOTS, HOOK, PICKLOCK, PICKAXE
};

#include "entity.h"

class Scene;
class Slash;
class Interface;

class Seeker : public Entity {

    protected:

        // Player data
        float crouchSpeed = 0.5f;
        int combo = 0;
        bool attacking = false;
        bool usingItem = false;
        int dropTimerMax = 16;
        int dropTimer = 16;

        // Projection sprites
        bn::sprite_item current = bn::sprite_items::s_seeker_bronze;
        constexpr inline static bn::sprite_item bronze = bn::sprite_items::s_seeker_bronze;
        constexpr inline static bn::sprite_item silver = bn::sprite_items::s_seeker_silver;
        constexpr inline static bn::sprite_item golden = bn::sprite_items::s_seeker_golden;

        // Attack
        Slash* slash;


    public:

        // Interface
        Interface* interface;

        // Warp
        bool warped = false;
        int warpIdx = 1;
        int warpX = 64;
        int warpY = 430;

        // Public stats
        int hpMax = 3;
        int hp = 3;
        int mpMax = 8;
        int mp = 8;
        int coin = 0;
        bool crouched = false;
        int damage = 1;

        // Items
        ItemSlot slotL = SHIELD;
        ItemSlot slotR = NONE;
        int swordLv = 0;
        int shieldLv = 0;
        bool shield = false;
        bool boots = false;
        bool hook = false;
        bool picklock = false;
        bool pickaxe = false;

        Seeker(Scene *s);

        void Update() override;
        
        void Move(bool r);

        void Flinch(bool d);
        
        void Stop();

        void CastSlash();
        
        void Jump();
        
        void Crouch(bool c);
        
        void Attack();
        
        void UseItem(bool r);
        
        void ChangeHP(int value);
        
        int GetHP();
        
        void Fall();

        void UpdateState() override;

        void SetState(ActionState s);

        Scene* GetScene();

        void LoadScene();
        
        void Flip(bool flipped);
        
        void Warp(int i, int x, int y);
};