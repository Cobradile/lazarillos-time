#pragma once

#include "bn_sprite_items_s_doncella.h"

#include "npc.h"

class Seeker;

class Doncella : public NPC {

    public:

        Doncella(int x, int y, Seeker *s);

        void SetAnim(int idx) override;

        void UpdateState() override;
};