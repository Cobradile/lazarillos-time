#pragma once

#include "bn_sprite_items_warp.h"

#include "entity.h"

class Seeker;

class Warp : public Entity {

    protected:

        // Warp data
        Seeker* seeker;
        int warpIdx;
        int warpX;
        int warpY;


    public:

        Warp(int x, int y, Seeker *s, int wIdx, int wX, int wY) : Entity(bn::sprite_items::warp, x, y, s->GetScene()), seeker(s), warpIdx(wIdx), warpX(wX), warpY(wY) {
            limitN = 88;
            limitW = 4;
            limitS = 8;
            limitE = 4;
        }

        void Update() override {
            // Check if the seeker collides with the warp
            if (seeker->EntityCollides(posX, posY, limitN, limitW, limitS, limitE)) {
                seeker->Warp(warpIdx, warpX, warpY);
            }

            if (!grounded) {
                // Apply gravity
                if (velY < maxVelY) velY += gravity;
            }

            UpdatePos();
            CheckColTile();
        }
};