#pragma once

#include "bn_sprite_items_s_perro.h"

#include "foe.h"

class Seeker;

class Jabali : public Foe {

    protected:


    public:

        Jabali(int x, int y, Seeker *s);

        void Update() override;

        void Move();

        void Stop();

        void UpdateState() override;

        void SetState(ActionState s);
};