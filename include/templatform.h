#pragma once

#include "bn_sprite_items_p_full_2.h"
#include "bn_sprite_items_p_full_3.h"
#include "bn_sprite_items_p_hurt_2.h"
#include "bn_sprite_items_p_hurt_3.h"
#include "bn_sprite_items_p_gone_2.h"
#include "bn_sprite_items_p_gone_3.h"

#include "entity.h"

class Seeker;

class Templatform : public Entity {

    protected:

        // Platform data
        Seeker* seeker;
        int size;

        // Countdown
        int countdownMax;
        int countdown;
        bool counted = false;

        // Countup
        int countupMax = 200;
        int countup = 0;

        // Leveled sprites
        bn::sprite_item full = bn::sprite_items::p_full_2;
        bn::sprite_item hurt = bn::sprite_items::p_hurt_2;
        bn::sprite_item gone = bn::sprite_items::p_gone_2;


    public:

        // So the main loop knows when to take it away
        bool departed = false;

        Templatform(int x, int y, Seeker *s, int size, int count) : Entity(bn::sprite_items::p_full_2, x, y, s->GetScene()), seeker(s), countdownMax(count), countdown(count) {

            switch(size) {
                case 2:
                    limitN = 12;
                    limitW = 8;
                    limitS = -7;
                    limitE = 8;
                    full = bn::sprite_items::p_full_2;
                    hurt = bn::sprite_items::p_hurt_2;
                    gone = bn::sprite_items::p_gone_2;
                    break;
                case 3:
                    limitN = 20;
                    limitW = 16;
                    limitS = -15;
                    limitE = 8;
                    full = bn::sprite_items::p_full_3;
                    hurt = bn::sprite_items::p_hurt_3;
                    gone = bn::sprite_items::p_gone_3;
                    break;
            }
            
            projection.set_item(full);
        }

        void Update() override {
            if (!departed) {
                if (!seeker->overgrounded && seeker->GetVel(false) > 0 && seeker->EntityCollides(posX, posY, limitN, limitW, limitS, limitE) && seeker->GetPos(false) <= GetPos(false) - (limitN-1)) {
                    if (!counted) {
                        projection.set_item(hurt);
                        countdown = 0;
                        counted = true;
                    }
                    seeker->overgrounded = true;
                }

                if (countdown < countdownMax) {
                    countdown++;
                    if (countdown == countdownMax-20) projection.set_item(gone);
                    if (countdown == countdownMax) {
                        projection.set_item(bn::sprite_items::empty);
                        departed = true;
                        if (seeker->overgrounded && seeker->EntityCollides(posX, posY, limitN, limitW, limitS, limitE)) {
                            seeker->overgrounded = false;
                        }
                    }
                }

                if (seeker->overgrounded && !seeker->EntityCollides(posX, posY, limitN, limitW, limitS, limitE)) {
                    seeker->overgrounded = false;
                }
            } else {
                countup++;
                if (countup == countupMax) {
                    projection.set_item(full);
                    departed = false;
                    counted = false;
                    countup = 0;
                }
            }
        }
};